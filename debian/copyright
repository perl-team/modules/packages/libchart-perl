Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Herbert Breunung <lichtkind@cpan.org>
Source: https://metacpan.org/release/Chart
Upstream-Name: Chart

Files: *
Copyright: 1997-1998, David Bonner
 1999, Peter Clark
 2001, Chart-Group <chartgrp@web.de>
 2022, Herbert Breunung and Chart group
License: Artistic or GPL-1+

Files: lib/Chart/Mountain.pm
Copyright: 1998-1999, James F. Miner
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2000, Raphael Bossek <bossekr@debian.org>
 2001, Rémi Perrot <rperrot@debian.org>
 2002, 2003, Stephen Zander <gibreel@debian.org>
 2006-2022, gregor herrmann <gregoa@debian.org>
 2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2010, Jotam Jr. Trejo <jotamjr@debian.org.sv>
 2012, Florian Schlichting <fsfs@debian.org>
 2012, Xavier Guimard <yadd@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
